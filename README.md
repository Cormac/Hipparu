# What is Hipparu?
 
Hipparu is a replacement for the venerable CSUS.edu Drag-n-Drop exercises that can be found here: 
 - https://www.csus.edu/indiv/s/sheaa/projects/genki/katakana-timer.html
 - https://www.csus.edu/indiv/s/sheaa/projects/genki/hiragana-timer.html
 
The main difference between the two is that you may swap freely between alphabets, rather than having two different applications for the exercises. 
 
# Why do they need replaced?

Flash will no longer be supported from the end of 2020, and the average Japanese learner who might have enjoyed this resource might not have the know-how to run a local emulator. It's just simpler to make a more modern solution. 

# Can I help?

Sure, make a pull request or open an issue with what you'd like to see.

# Where is it hosted?

Nowhere for now. I'll get some hosting for it sorted before the Flash end-of-life. The CSUS.edu solution will work just fine until then. Incidentally if you know anywhere decent (and cheap!) for me to host it, open up an issue.

がんばってください！
